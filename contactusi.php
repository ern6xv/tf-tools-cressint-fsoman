<html>
<body>
<?php
// ========================================================================
//
//   CressInt contactusi.php 
//		Processes contactform.html.twig - the Contact Form for the Weirauch Lab
//			Cressint Arabidopsis thaliana Intersector Tool
//  	Author:  F.Soman
//  
//
// ========================================================================
//
//	REMOVE comments from lines 30-31 to send emails to the TF tools administrator
//  REMOVE lines 33 and 36 to the temporary cressint admin email address

ini_set('sendmail_from', 'crstooladm@outlook.com');
ini_set('smtp_port','587');
ini_set('imap_port', '993');
ini_set('SMTP','smtp-mail.outlook.com' ); 


//  name, email, sub, contact_comments initialization

$emailErr = "";
$email = "";
$contact_comment = "";
$contact_comment_cleaned = "";
$name = "";
$sub = "";
// $admin_from = "2tftoolsadmin@cchmc.org";
// $name_to = "tftoolsadmin@cchmc.org"; 
$name_to = "crstooladm@outlook.com";
$subject_folder = "Cressint_Contact_Messages";
$reply_to = "";
$admin_from = "2crstooladm@outlook.com";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$valid = 'true';
	
		$name = test_input($_POST["name"]);
		
		$email = test_input($_POST["email"]);
		filter_var($email, FILTER_SANITIZE_EMAIL);
		if(!filter_var($email, FILTER_VALIDATE_EMAIL))  {
			$emailErr = "\nFormat <Youremail@youremailprovider.com>";
			$valid = 'false';
			}
		else	{
			$reply_to = $email;
			}
		
	$sub = $_POST["subject"];
	$contact_comment = test_input($_POST["contact_comment"]);
	$contact_comment_cleaned = preg_replace('/[^ \w]+/','',$contact_comment);

if ($valid == 'true') {
	
	$message = "Message from: " . $name . " at " . $email . "\n\n" . $sub . "\n\n" . $contact_comment_cleaned;

	$headers = "From: " . $admin_from . "\nReply_To: " . $reply_to . "\nX-Mailer: PHP/" . PHP_VERSION;
	
	  if (mail($name_to, $subject_folder, $message, $headers) == TRUE)	{
		echo ("<p>\nThank you for your message.</p>");

		}
	  else	{
		echo ("<p>\n Sorry, an error occurred when sending your message...please try again later.</p>");
 
		}
	  
	}
}
   

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
?>
</body>
</html>