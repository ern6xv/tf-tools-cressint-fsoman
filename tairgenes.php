<?php
if ( !isset($_REQUEST['term']) ) {
    $form = <<<HTML
<!DOCTYPE html>
<html>
<head>
  <title>test jquery autocomplete</title>
  <script type="text/javascript" src="/assets/vendor/jq/jquery.min.js"></script>
  <script type="text/javascript" src="/assets/vendor/jqui/jquery-ui.min.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($){
      $('#genesearch').autocomplete({ source: 'tairgenes.php', minLength: 1 });
    });
  </script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <!--link rel="stylesheet" href="/assets/vendor/jqueryui/jquery-ui.theme.min.css"-->
  <style type="text/css">
     /* style the auto-complete response */
     li.ui-menu-item { font-size:12px !important; }
  </style>
</head>

<body>
<form onsubmit="return false;">
Enter a Gene Name:
<input id="genesearch" type="text" />
</form>

</body>
</html>
HTML;
    echo $form;
    exit;
}

// Otherwise...
$TABLE  = 'gene_aliases';
if (getenv('TF_TOOLS_ENV') === 'dev') {
    ini_set('display_errors', '1');
    $DBHOST = 'bmirschd04my';
    $DB     = 'bedintersect';
    $DBUSER = 'srv_bedintersect';
    $DBPASS = getenv('FUGIT_DB_PASS');
} else {
    $DBHOST = 'localhost';
    $DB     = 'cressint';
    $DBUSER = 'cressint';
    $DBPASS = getenv('CRESSINT_DB_PASS');
}

$dblink = mysql_connect($DBHOST, $DBUSER, $DBPASS) or die( mysql_error() );
mysql_select_db($DB);

$term = mysql_real_escape_string($_REQUEST['term']);
$query = <<<SQL
    select locus_name, symbol, full_name from $TABLE
    where locus_name like '%$term%' or symbol like '%$term%'
        or full_name like '%$term%'
    order by locus_name asc limit 0,10;
SQL;

$rs = mysql_query($query, $dblink);

$data = array();
if ( $rs && mysql_num_rows($rs) ) {
    while ( $row = mysql_fetch_array($rs, MYSQL_ASSOC) ) {
        $data[] = array(
            'label' => $row['locus_name']
                    .  ( empty($row['symbol'])    ? '' : " (${row['symbol']})" )
                    .  ( empty($row['full_name']) ? '' : " - ${row['full_name']}" ),
            'value' => $row['locus_name']
        );
    }
} // if recordset was non-empty

echo json_encode($data);
flush();

// tairgenes.php
