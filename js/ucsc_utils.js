var UCSCutils = UCSCutils || (function(){
  'use strict';
  var HGSID = '443985234_i7NrQsS3ia10gvwUq55I4PZHls90';
  var PAD = 3000; // bases (on either side)
  var TIPTMPL = 'Open 6 Kbase region around this SNP/sequence (%s:%i-%i) in the UCSC Genome Browser';
  var URLBASE = 'http://genome.ucsc.edu/cgi-bin/hgTracks?db=hub_10649_araTha1&' +
                'hgsid=' + HGSID;

  // Source: http://www.ncbi.nlm.nih.gov/assembly/237408#/st
  // NB: These *might* change when 'tair11' comes out.
  var end_of = {
    'chr1': 30427671,
    'chr2': 19698289,
    'chr3': 23459830,
    'chr4': 18585056,
    'chr5': 26975502
  };

  return {
    /**
     *  @brief DataTables 'mRender' function returning a link to UCSC GB for
     *
     *  See also: http://legacy.datatables.net/ref (search for 'mRender')
     *
     *  @param  data   the cell's contents (defaults to the same array index
     *                 as you specified in 'aTargets')
     *  @param  type   either 'display', 'sort', 'filter', or 'type'; in our
     *                 case, we only want to wrap the cell contents in the
     *                 'display' mode
     *  @param  full   the entire row's source data (as an array)
     *  @return        the cell's contents replaced with a link to the UCSC
     *                 Genome Browser at +/- 3 Kbases to either side of the
     *                 SNP coordinate, with our CressInt custom track already
     *                 loaded.
     */
    wrap_location_with_link: function(data, type, full) {
      var chr   = full[0].toLowerCase();
      var start = (+full[1]-PAD < 1)           ? 1           : +full[1]-PAD;
      var end   = (+full[2]+PAD > end_of[chr]) ? end_of[chr] : +full[2]+PAD;
      var pos   = chr + ':' + start + '-' + end;
      var tip   = TIPTMPL.replace('%s', chr).replace('%i', start).replace('%i', end);
      var title = '';
      
      if (type === 'display') {
        // Don't show the tooltip except for the "Chr" column; the "From" and
        // "To" columns might have been constrained by 'tabletools.js' and the
        // tooltip would display their full contents. We need to preserve
        // this.
        if (data.toLowerCase() === chr) {
          title = 'title="' +tip+ '"';
        }
        return '<a ' +title+ ' href="' +URLBASE+ '&position=' +pos+
               '" target="_blank">' +data+ '</a>';
      } else {
        return data;
      } // wrap in A tag for type=='display'; return data unchanged otherwise

    } // wrap_ucsc_gb_link
  };
}()); // UCSCutils
