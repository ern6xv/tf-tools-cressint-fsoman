##Modifications to add FASTA input and email contact form
**Author**: Frances Soman

**Contains**:
Contact form functionality and Additional FASTA Conversion work
requested by Dr. Weirauch for Weirauch Lab CressInt _Arabidopsis thaliana_
Intersector Tool (paper submitted for publication April 15, 2015) for my
Weirauch Lab rotation, Feb. 10-July 31, 2015.

##FASTA Conversion Tool
This tool is configured to run in the CressInt TWIG Environment as a command
line tool for the CressInt homepage FASTA Sequence(s) input button and as a
Standalone FASTA Conversion tool to be added to the Cressint Tools and
Portals.  It has been thoroughly tested outside of the Cressint TWIG
environment, but not in the Cressint TWIG Environment, with Cressint header,
navigation bar and footer.

###Stand Alone FASTA to BED Conversion Tool
For Cressint Tools and Portals:

    fastaform.php
    fastaform.html.twig
    fasta2bed.php
    fastaresults.php
    facressint.css

###Command Line FASTA to BED Conversion

####Files supplied for Cressint homepage Fasta Sequence(s) button:

    fasta.php
    fasta2bed.php
    fastaresults.php
    facressint.css

FASTA Conversion Tool FASTA files, conversion screen shots and examples.

##Cressint Contact Form and form functionality files:

    contactus.php (already submitted, copy here)
    contactus.html.twig (already submitted, modified here)
    contactusi.php

Contact form function screen shots and example email.

##Description
###CressInt Contact Form

Contact Us form for user comments, processed to clean and validate user input,
and prepare a comment email sent to the administrator email address into a
Cressint_Contact_Messages folder.

This is currently sending the contact email to a temporary cressint tool
administrator outlook email address.  As indicated at the top of the
`contactusi.php file`, the administrator email address will be changed to the
tftoolsadmin email address.

###FASTA Conversion Tool

Includes Stand-alone FASTA Conversion Tool and command-line FASTA Conversion
Tool

This tool converts FASTA sequence files or pasted FASTA sequence(s) to a BED 6
file.  The tool is currently configured for conversion of Aradopsis Thaliana
sequences for use in the Weirauch Lab (WRL) CressInt Arabidopsis thaliana
Intersector Tool (see above).

This tool accepts as input DNA, RNA, or Protein sequences in a file or in
pasted sequence(s).  The tool automatically determines the type of FASTA
sequence(s) submitted.

The command line version requires the CressInt homepage FASTA Sequence
function to:

- send in the command line to the Conversion Tool processor the FASTA Sequence
  file name to upload or pasted sequence(s) from the user
- use the resultant BED file for input into the CressInt Intersector homepage.

The standalone version allows the user to indicate the FASTA sequence file to
upload or paste in sequence(s), and allows the user to optionally indicate the
sequence type.

##Installation Instructions

###FASTA Conversion Tool

1. The following files should be placed in the `/var/www/html/cressint`
   folder: `fastaform.php`
2. The following files should be placed in the
   `/var/www/html/cressint/templates` folder: `fastaform.html.twig`,
   `fasta.php`, `fasta2bed.php`, `fastaresults.php`
3. The following Arabidopsis genome files should be placed in the
   `/var/www/html/cressint` folder: `araTha1.2bit`, `AT_10pep.fa`
4. Files `blat` and `pslToBed` in the `FASTAConversionTool/scripts` folder
   should be placed in the `/var/www/html/cressint/scripts` folder.
5. The FASTA css file, `facressint.css` in the `FASTAConversionTool/css`,
   should be placed in the `/var/www/html/cressint/css` folder.
6. FASTA Conversion results require use of the `/var/www/html/cressint/cache`
   folder.
7. Comments should be removed from surrounding the TWIG statements in all
   blocks in `fastaform.html.twig` and `fasta2bed.php`
8. Follow the directions at the top of the `fastaform.html.twig` file to
   remove comments surrounding the TWIG statements, and `fasta2bed.php` file
   to remove comments from surrounding the TWIG statements and from the `$cfg`
   statement sections and remove the indicated GLOBALS statements.
9. Add to the CressInt Tools and Portals web page the link for
   `/var/www/html/cressint/fastaform.php` to render the standalone FASTA
   Conversion Tool.

###Cressint Contact Us form:

1. The `contactus.php` file which renders the `contactform.html.twig` should
   already be in the `/var/www/html/cressint` folder.
2. The following files should be placed in the
   `/var/www/html/cressint/templates` folder: `contactform.html.twig`,
   `contactusi.php`
3. Comments should be removed from surrounding the TWIG statements in all
   blocks of the `contactform.html.twig` file.
4. As indicated in `contactusi.php`, remove the comments from the
   tftoolsadmin@cchmc.org and 2tftoolsadmin@cchmc.org email address lines and
   remove lines with the temporary cressint admin email address. 
5. Change `sidebar.html.twig` link to `contactus.php`, as before, to render
   the `contactform.html.twig`.

