<!---
//*******************************************************************************//
//                                                                               //
//	CressInt Arabidopsis thaliana Intersector FASTA to BED Conversion Tool       /
//  fastaresults.php   FASTA Conversion Results Download                                 /                     
//                                                                               /
//    Author:  Frances Soman                                                     /
//	                                                                             /
//*******************************************************************************//
--->
<html>
<head>
<?php
{
	$v = $_GET['var1'];
	$v2 = $_GET['var2'];
	if (!file_exists($v)) {
		echo "v exists";
	}
down_load($v, $v2);
return;
}

function down_load($original, $new_f)	{

	ob_clean();
	//flush();
	header("Cache-Control: no-cache, must-revalidate");
	header("Pragma: no-cache");
	header("Expires: 0"); 
	header("Content-Type: application/octet-stream");
	header("Content-Length: " . filesize($original));
	header('Content-Disposition: attachment; filename="' . $new_f . '"');
	echo 'reading file';
	readfile($original);
	ob_flush();
	flush();
	return;
	}
?>	
</head>
<body>
</body>
</html>